package db

import (
	"encoding/json"
	"fmt"
)

type Config struct {
	DB *DB
}

func NewConfig(db *DB) *Config {
	return &Config{db}
}

func (s *Config) bucket() []byte {
	return []byte("config")
}

func (s *Config) Put(key string, value string) error {
	err := s.DB.Update(func(tx *Tx) error {
		b := tx.Bucket(s.bucket())
		return b.Put([]byte(key), []byte(value))
	})

	return err
}

func (s *Config) Get(key string) (string, error) {
	var val string = ""

	err := s.DB.View(func(tx *Tx) error {
		b := tx.Bucket(s.bucket())
		val = string(b.Get([]byte(key)))
		return nil
	})

	return val, err
}

func (s *Config) ListAll() map[string]string {

	var config map[string]string

	s.DB.View(func(tx *Tx) error {
		b := tx.Bucket(s.bucket())
		c := b.Cursor()

		for k, v := c.First(); k != nil; k, v = c.Next() {
			config[string(k)] = string(v)
		}

		return nil
	})
	fmt.Println(config)
	return config
}

func (s *Config) MarshalIndent() ([]byte, error) {
	config := s.ListAll()
	return json.MarshalIndent(config, "", "    ")
}
