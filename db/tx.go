package db

import (
	"github.com/boltdb/bolt"
)

// Tx represents a BoltDB transaction
type Tx struct {
	*bolt.Tx
}

// Record retrieves a Record from the database with the given name.
func (tx *Tx) Record(name []byte) (*Record, error) {
	r := &Record{Tx: tx, Name: name}
	return r, r.Load()
}

func (tx *Tx) Server(addr []byte) (*Server, error) {
	s := &Server{Tx: tx, Addr: addr}
	return s, s.Load()
}

func (tx *Tx) ScanServers() ([]string, error) {
	s := &Server{Tx: tx}
	b := tx.Bucket(s.Bucket())
	list := make([]string, 0, 50)
	b.ForEach(func(k, v []byte) error {
		// If there's a value, it's not a bucket so ignore it.
		if v != nil {
			list = append(list, string(k))
		}
		return nil
	})
	return list, nil
}
