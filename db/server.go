package db

import (
	"encoding/json"
	"time"
)

type ServerData struct {
	Addr     []byte    `json:"addr"`
	LastSeen time.Time `json:"lastSeen"`
}

// Server represents a server in the ring
type Server struct {
	Tx         *Tx
	Addr       []byte
	ServerData *ServerData
}

func (s *Server) Bucket() []byte {
	return []byte("servers")
}

func (s *Server) Update(addr []byte) {
	s.ServerData = &ServerData{
		Addr:     addr,
		LastSeen: time.Now(),
	}
}

func (s *Server) get() ([]byte, error) {
	value := s.Tx.Bucket(s.Bucket()).Get(s.Addr)
	if value == nil {
		return nil, &Error{"server not found", nil}
	}
	return value, nil
}

// Load retrieves a page from the database.
func (s *Server) Load() error {
	value, err := s.get()
	if err != nil {
		return err
	}

	data := &ServerData{}
	err = json.Unmarshal(value, data)
	if err != nil {
		return err
	}
	s.ServerData = data

	return nil
}

func (s *Server) JSON() ([]byte, error) {
	return json.MarshalIndent(s.ServerData, "", "    ")
}

// Save commits the Record to the database.
func (s *Server) Save() error {
	assert(len(s.Addr) != 0, "uninitialized server cannot be saved")
	value, err := json.Marshal(s.ServerData)
	if err != nil {
		return err
	}
	return s.Tx.Bucket(s.Bucket()).Put(s.Addr, value)
}
