package db

import (
	"encoding/json"
)

// Record represents a key-value pair
type Record struct {
	Tx    *Tx    `json:"-"`
	Name  []byte `json:"name"`
	Value []byte `json:"value"`
}

func (r *Record) bucket() []byte {
	return []byte("records")
}

func (r *Record) get() []byte {
	return r.Tx.Bucket(r.bucket()).Get(r.Name)
}

// Load retrieves a page from the database.
func (r *Record) Load() error {
	r.Value = r.get()
	return nil
}

func (r *Record) JSON(useString bool) ([]byte, error) {
	if useString {
		return json.MarshalIndent(struct {
			Name  string `json:"name"`
			Value string `json:"value"`
		}{
			Name:  string(r.Name),
			Value: string(r.Value),
		}, "", "    ")
	} else {
		return json.MarshalIndent(struct {
			Name  string `json:"name"`
			Value []byte `json:"value"`
		}{
			Name:  string(r.Name),
			Value: r.Value,
		}, "", "    ")
	}
}

// Save commits the Record to the database.
func (r *Record) Save() error {
	assert(len(r.Name) != 0, "uninitialized page cannot be saved")
	return r.Tx.Bucket(r.bucket()).Put(r.Name, r.Value)
}
