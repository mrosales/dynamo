# Dynamo Style Read/Write Replication

While I try to provide general instructions on how to get this working, the truth is that a lot of this setup probably has unique querks to my development environment and will require a lot of tweaks to get running from another system.


## Getting Started
This project requires a working, configured AWS Elastic Container Service Cluster. Assuming you have your AWS credentials properly set up, spinning up a service is as simple as:


```
CLUSTER_SIZE = 8
ecs-cli up --keypair id_rsa --capability-iam --size $CLUSTER_SIZE --instance-type t2.micro
```

Then you can spin up instances of this project by navigating to the root of the project folder and running:

```
./docker-up.sh
```

You can tweak the parameters of the system by modifying the arguments in `./docker-up.sh`.

*Note: using `docker-up.sh` requires that the `dynamo-client` program is loaded. To load this program, execute the commands below*

```
go get -u "bitbucket.org/mrosales/dynamo-client"
cd "$GOPATH/src/bitbucket.org/mrosales/dynamo-client"
go build && go install
```

With `dynamo-client`, you can also run `deploy --logs` to connect to the root instance of the clusted and see the logs as requests are being fullfilled.

## The Source

The entry point is [`main.go`](main.go). Upon entry, the program creates a DB connection for the local key-value store and then starts and RPC server and a HTTP Rest API server and registers the associated routes.

## Configuration

Each node stores information about itself and also maintains a list of all of the other nodes. After the instance has been started, it remains in an uninitialized mode. Until the `dynamo-client` deploy application register's its information. `dynamo-client` uses the AWS CLI to download all of the information about an all of the instances in the cluster and then it relays each node's information to its corresponding node. This is part of the deployment process for the ECS cloud.

The `dynamo-client` also selects a random node and chooses it to be the root node. It then tells all of the other nodes to join the root node. Each time the root receives a join request, it tells all of the other nodes to update their server lists. At the end of the `dynamo-client`'s deploy process, we are left with a network of nodes that all have valid and up-to-date server lists. The server list consists of the node's IP address (internal to the ECS VPC) and the port which it communicates on via RPC.

The configuration information is public and is viewable via `http://NODE_PUBLIC_IP/config` and `http://NODE_PUBLIC_IP/config/node`

## Local Key-Value Store

The program is build on BoltDB, a database entirely written in GO. The database is low level and has been selected for its minimal key-value interface rather than for performance reasons. The files in the [`db` package](db) provide wrappers for commonly used data types around the bolt interface.


## RPC Interface

Most of the logic for the request handling is contained within the [`RPC` package](rpc). This package runs the RPC interface that handles communication to and from other nodes. [`rpc-client.go`](rpc/rpc-client.go) is the public interface used by `dynamo-client` to issue `Get()` and `Put()` requests, and also has the interfaces for internal node-to-node communication. The logic for request coordination is in [`rpc/coordinator.go`](rpc/coordinator.go)
