package main

import (
	"bitbucket.org/mrosales/dynamo/config"
	"bitbucket.org/mrosales/dynamo/db"
	"bitbucket.org/mrosales/dynamo/rpc"
	"bitbucket.org/mrosales/dynamo/servers"
	"bitbucket.org/mrosales/dynamo/stash"
	"github.com/gorilla/mux"
	grpc "github.com/gorilla/rpc"
	"github.com/gorilla/rpc/json"
	"github.com/zenazn/goji"
	"log"
	"net"
	"net/http"
	"os"
	"strconv"
)

// constants defined in docker-compose file
const (
	CONN_HOST = ""
	CONN_PORT = "8000"
	CONN_TYPE = "tcp"
	RPC_PORT  = "9000"
)

// holds config parsed from local environment
type EnvConfig struct {
	N      int
	R      int
	W      int
	DBPath string
}

func main() {

	env := &EnvConfig{}
	env.Parse()

	var db db.DB
	if err := db.Open(env.DBPath, 0600); err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	servers, err := servers.NewServers(&db)
	if err != nil {
		log.Fatal(err)
	}

	stash, err := stash.NewStash(&db, servers, env.N, env.R, env.W)
	if err != nil {
		log.Fatal(err)
	}

	config := config.NewConfig(&db, stash)
	if err != nil {
		log.Fatal(err)
	}
	config.RestoreFromDisk()

	log.Println("Created stash and opened db.")

	// run rpc server asynchronously
	go rpc_server(stash, servers, config)

	// runs rest server -> blocks until exit
	rest_server(stash, servers, config)
}

func rest_server(st *stash.Stash, sr *servers.Servers, cfg *config.Config) {
	goji.Get("/", sr.ScanREST)

	goji.Get("/servers", sr.ScanREST)
	goji.Post("/servers", sr.PutREST)
	goji.Get("/servers/:addr", sr.ReadREST)
	goji.Delete("/servers/:addr", sr.DeleteREST)

	goji.Get("/stash/:name", st.Read)
	goji.Post("/stash/:name", st.Update)

	goji.Post("/config/ip", cfg.RegisterInstanceHandler)
	goji.Get("/config", cfg.ListREST)
	goji.Get("/config/node", cfg.NodeREST)

	// Start the web server
	l, err := net.Listen(CONN_TYPE, ":"+CONN_PORT)
	if err != nil {
		log.Fatal("Error listening:", err.Error())
	}
	// Close the listener when the application closes.
	defer l.Close()
	goji.ServeListener(l)
}

func rpc_server(st *stash.Stash, sr *servers.Servers, cfg *config.Config) {
	s := grpc.NewServer()
	s.RegisterCodec(json.NewCodec(), "application/json")
	s.RegisterCodec(json.NewCodec(), "application/json;charset=UTF-8")

	// register rpc services

	serverService := rpc.NewServerService(sr, cfg)
	s.RegisterService(serverService, "")

	stashService := rpc.NewStashService(st)
	s.RegisterService(stashService, "")

	configService := rpc.NewConfigService(cfg, sr)
	s.RegisterService(configService, "")

	// set up routes to proper services
	r := mux.NewRouter()
	r.Handle("/rpc", s)

	// create tcp listener on random open port
	l, err := net.Listen("tcp", ":"+RPC_PORT)
	if err != nil {
		log.Fatal("Error listening for RPC:", err.Error())
	}
	// Close the listener when the application closes.
	defer l.Close()

	// log port number and start serving on listener
	port := l.Addr().(*net.TCPAddr).Port
	log.Printf("Started rpc server on port %d\n", port)
	http.Serve(l, r)
}

func (c *EnvConfig) Parse() {
	n := os.Getenv("N")
	if n != "" {
		val, _ := strconv.ParseInt(n, 0, 64)
		c.N = int(val)
	} else {
		c.N = 3
	}

	r := os.Getenv("R")
	if r != "" {
		val, _ := strconv.ParseInt(r, 0, 64)
		c.R = int(val)
	} else {
		c.W = 2
	}

	w := os.Getenv("W")
	if w != "" {
		val, _ := strconv.ParseInt(w, 0, 64)
		c.W = int(val)
	} else {
		c.W = 2
	}

	dbPath := os.Getenv("DB_PATH")
	if dbPath != "" {
		c.DBPath = dbPath
	} else {
		c.DBPath = "test.db"
	}

}
