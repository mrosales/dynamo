package stash

import (
	"bitbucket.org/mrosales/dynamo/db"
	"bitbucket.org/mrosales/dynamo/servers"
	"fmt"
)

type Stash struct {
	db                *db.DB
	Servers           *servers.Servers
	NumReplicas       int
	ReadParticipants  int
	WriteParticipants int
}

func NewStash(db *db.DB, srv *servers.Servers, n int, r int, w int) (*Stash, error) {
	s := &Stash{
		db:                db,
		Servers:           srv,
		NumReplicas:       n,
		ReadParticipants:  r,
		WriteParticipants: w,
	}
	return s, nil
}

func (s *Stash) Get(key []byte) ([]byte, error) {

	var value []byte = nil
	err := s.db.View(func(tx *db.Tx) error {
		r, err := tx.Record(key)

		if err != nil {
			return err
		}
		value = r.Value
		return nil
	})

	return value, err
}

func (s *Stash) Put(key []byte, value []byte) bool {

	err := s.db.Update(func(tx *db.Tx) error {
		p := db.Record{Tx: tx, Name: key, Value: value}
		fmt.Printf("Updating Record: '%s'='%s'\n", p.Name, p.Value)
		return p.Save()
	})

	return err == nil
}
