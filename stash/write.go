package stash

import (
	"encoding/json"
	"github.com/zenazn/goji/web"
	"net/http"
)

type requestData struct {
	Value string `json:"value"`
}

// Update is the update endpoint of the Wiki
func (s *Stash) Update(c web.C, rw http.ResponseWriter, r *http.Request) {
	name := []byte(c.URLParams["name"])

	if r.Header.Get("Content-Type") != "application/json" {
		rw.WriteHeader(http.StatusUnsupportedMediaType)
		return
	}

	var rd requestData
	err := json.NewDecoder(r.Body).Decode(&rd)
	if err != nil {
		// Unprocessable Entity
		rw.WriteHeader(422)
		return
	}

	s.Put(name, []byte(rd.Value))

	// rw.Header().Set("Location", s.UriForRecord(string(name)))
	rw.WriteHeader(http.StatusCreated)
}
