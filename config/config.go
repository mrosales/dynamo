package config

import (
	"bitbucket.org/mrosales/dynamo/db"
	"bitbucket.org/mrosales/dynamo/stash"
	"encoding/json"
	"fmt"
	"github.com/zenazn/goji/web"
	"net/http"
	"os"
	"reflect"
	"strconv"
)

type NodeConfig struct {
	IsRoot            bool   `json:"isRoot"`
	RootAddr          string `json:"rootAddr"`
	RootRPCPort       int64  `json:"rootRPCPort"`
	IsInitialized     bool   `json:"isInitialized"`
	NumReplicas       int64  `json:"numReplicas"`
	ReadParticipants  int64  `json:"readParticipants"`
	WriteParticipants int64  `json:"writeParticipants"`
}

type InstanceData struct {
	LocalIP                string `json:"localIP"`
	PublicIP               string `json:"publicIP"`
	RPC_PORT               string `json:"RPCPort"`
	HTTP_PORT              string `json:"HTTPPort"`
	ECSContainerInstanceId string `json:"ecsContainerInstanceId"`
	EC2InstanceId          string `json:"ec2InstanceId"`
}

type Config struct {
	dbConfig     *db.Config
	InstanceData *InstanceData
	NodeConfig   *NodeConfig
	stash        *stash.Stash
}

func NewConfig(myDB *db.DB, myStash *stash.Stash) *Config {
	return &Config{dbConfig: db.NewConfig(myDB), stash: myStash}
}

func (cfg *Config) RegisterInstanceHandler(c web.C, rw http.ResponseWriter, r *http.Request) {

	if r.Header.Get("Content-Type") != "application/json" {
		rw.WriteHeader(http.StatusUnsupportedMediaType)
		return
	}

	var id InstanceData
	err := json.NewDecoder(r.Body).Decode(&id)
	if err != nil {
		// Unprocessable Entity
		rw.WriteHeader(422)
		return
	}

	fmt.Printf("/registerIP   %+v\n", id)
	cfg.RegisterInstance(&id)

	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		return
	}

	rw.WriteHeader(http.StatusOK)
}

func (c *Config) RestoreNodeConfig() {
	data := NodeConfig{}
	val := reflect.ValueOf(data)
	for i := 0; i < val.NumField(); i++ {
		tag := val.Type().Field(i).Tag
		label := tag.Get("json")

		str, _ := c.dbConfig.Get(label)

		field := reflect.ValueOf(&data).Elem().Field(i)
		if field.Kind() == reflect.Bool {
			field.SetBool(str == "true")
		} else if field.Kind() == reflect.Int64 {
			iVal, _ := strconv.ParseInt(str, 0, 64)
			field.SetInt(iVal)
		} else {
			field.SetString(str)
		}
	}
	c.NodeConfig = &data
	c.stash.NumReplicas = int(data.NumReplicas)
	c.stash.ReadParticipants = int(data.ReadParticipants)
	c.stash.WriteParticipants = int(data.WriteParticipants)
}

func (c *Config) RestoreInstanceData() {
	data := InstanceData{}
	val := reflect.ValueOf(data)
	for i := 0; i < val.NumField(); i++ {
		tag := val.Type().Field(i).Tag
		label := tag.Get("json")

		str, _ := c.dbConfig.Get(label)

		field := reflect.ValueOf(&data).Elem().Field(i)
		if field.Kind() == reflect.Bool {
			field.SetBool(str == "true")
		} else if field.Kind() == reflect.Int64 {
			iVal, _ := strconv.ParseInt(str, 0, 64)
			field.SetInt(iVal)
		} else {
			field.SetString(str)
		}
	}
	c.InstanceData = &data
}

func (c *Config) SaveInterface(iface interface{}) bool {
	val := reflect.ValueOf(iface)

	for i := 0; i < val.NumField(); i++ {
		valueField := val.Field(i)
		label := val.Type().Field(i).Tag.Get("json")

		var str string
		if valueField.Kind() == reflect.Bool {
			str = strconv.FormatBool(valueField.Bool())
		} else if valueField.Kind() == reflect.Int64 {
			str = strconv.FormatInt(valueField.Int(), 10)
		} else {
			str = valueField.String()
		}

		c.dbConfig.Put(label, str)
	}
	return true
}

func (c *Config) RestoreFromDisk() {
	c.RestoreInstanceData()
	c.RestoreNodeConfig()
	os.Setenv("DYNAMO_ADDR", c.InstanceData.LocalIP+":"+c.InstanceData.RPC_PORT)
}

func (c *Config) RegisterInstance(idata *InstanceData) bool {
	c.InstanceData = idata
	os.Setenv("DYNAMO_ADDR", idata.LocalIP+":"+idata.RPC_PORT)
	c.SaveInterface(*idata)
	return true
}

func (c *Config) InitializeInstance(cfg *NodeConfig) bool {
	c.NodeConfig = cfg
	c.SaveInterface(*cfg)

	c.stash.NumReplicas = int(cfg.NumReplicas)
	c.stash.ReadParticipants = int(cfg.ReadParticipants)
	c.stash.WriteParticipants = int(cfg.WriteParticipants)

	return true
}

func (cfg *Config) NodeREST(c web.C, rw http.ResponseWriter, r *http.Request) {
	data := cfg.NodeConfig
	if data == nil {
		rw.WriteHeader(http.StatusNotFound)
		return
	}

	jResp, err := json.MarshalIndent(data, "", "    ")

	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		return
	}

	rw.Header().Set("Content-Type", "application/json")
	rw.Write(jResp)
	rw.WriteHeader(http.StatusOK)
}

func (cfg *Config) ListREST(c web.C, rw http.ResponseWriter, r *http.Request) {
	data := cfg.InstanceData
	if data == nil {
		rw.WriteHeader(http.StatusNotFound)
		return
	}

	jResp, err := json.MarshalIndent(data, "", "    ")

	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		return
	}

	rw.Header().Set("Content-Type", "application/json")
	rw.Write(jResp)
	rw.WriteHeader(http.StatusOK)
}
