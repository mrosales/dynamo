package servers

import (
	"encoding/json"
	"github.com/zenazn/goji/web"
	"log"
	"net/http"
)

func (s *Servers) DeleteREST(c web.C, rw http.ResponseWriter, r *http.Request) {
	addr := s.getServerName(c.URLParams["addr"])
	log.Printf("Deleting server record: %s\n", addr)
	err := s.Delete(addr)
	if err != nil {
		log.Printf("Error deleting server: %+v\n", err)
		rw.WriteHeader(http.StatusInternalServerError)
	} else {
		rw.WriteHeader(http.StatusNoContent)
	}
}

func (s *Servers) ReadREST(c web.C, rw http.ResponseWriter, r *http.Request) {
	addr := s.getServerName(c.URLParams["addr"])
	log.Printf("Searching for %s\n", addr)

	server, err := s.Read(addr)

	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		return
	}

	if server == nil {
		rw.WriteHeader(http.StatusNotFound)
		return
	}

	jResponse, err := server.JSON()
	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
	} else {
		rw.Header().Set("Content-Type", "application/json")
		rw.Write(jResponse)
		rw.WriteHeader(http.StatusOK)
	}
}

type requestData struct {
	Addr string `json:"addr"`
}

func (s *Servers) PutREST(c web.C, rw http.ResponseWriter, r *http.Request) {

	if r.Header.Get("Content-Type") != "application/json" {
		rw.WriteHeader(http.StatusUnsupportedMediaType)
		return
	}

	var rd requestData
	err := json.NewDecoder(r.Body).Decode(&rd)
	if err != nil {
		// Unprocessable Entity
		rw.WriteHeader(422)
		return
	}

	_, err = s.Add([]byte(rd.Addr))

	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		return
	}

	rw.Header().Set("Location", s.UriForServer(rd.Addr))
	rw.WriteHeader(http.StatusCreated)
}

func (s *Servers) ScanREST(c web.C, rw http.ResponseWriter, r *http.Request) {
	list, err := s.List()
	if err != nil {
		rw.WriteHeader(http.StatusNotFound)
		return
	}

	jResp, err := json.MarshalIndent(list, "", "    ")

	if err != nil {
		rw.WriteHeader(http.StatusInternalServerError)
		return
	}

	rw.Header().Set("Content-Type", "application/json")
	rw.Write(jResp)
	rw.WriteHeader(http.StatusOK)
}
