package servers

import (
	"bitbucket.org/mrosales/dynamo/db"
	"fmt"
	"github.com/serialx/hashring"
	"net/url"
	"sort"
)

type Servers struct {
	db         *db.DB
	serverList []string
	hashRing   *hashring.HashRing
}

func NewServers(db *db.DB) (*Servers, error) {
	s := &Servers{db: db}
	return s, nil
}

func (s *Servers) PreferenceListForKey(key string, listSize int) (nodes []string, ok bool) {

	if s.hashRing == nil {
		fmt.Println("Initializing HashRing")
		s.List()
	}

	return s.hashRing.GetNodes(key, listSize)
}

func (s *Servers) List() ([]string, error) {
	// simple memcache
	if s.serverList != nil {
		return s.serverList, nil
	}

	var serverAddr []string = nil

	err := s.db.View(func(tx *db.Tx) error {
		list, err := tx.ScanServers()
		serverAddr = []string(list)
		return err
	})

	sort.Strings(serverAddr)

	s.serverList = serverAddr
	s.hashRing = hashring.New(serverAddr)
	return serverAddr, err
}

/********************************************
   Interface for servers
********************************************/

func (s *Servers) Delete(addr []byte) error {
	return s.db.Update(func(tx *db.Tx) error {
		r, err := tx.Server(addr)
		if err != nil {
			return err
		}
		err = r.Tx.Bucket(r.Bucket()).Delete(addr)

		if err != nil {
			return err
		}
		s.serverList = nil // invalidate cache
		return nil
	})
}

func (s *Servers) Read(addr []byte) (*db.Server, error) {
	var server *db.Server = nil
	err := s.db.View(func(tx *db.Tx) error {
		r, err := tx.Server(addr)
		if err != nil {
			return err
		}
		server = r

		return nil
	})
	return server, err
}

func (s *Servers) Add(addr []byte) (*db.Server, error) {
	var server *db.Server = nil
	err := s.db.Update(func(tx *db.Tx) error {
		server = &db.Server{Tx: tx, Addr: addr}
		server.Update(addr)
		return server.Save()
	})
	s.serverList = nil // invalidate cache
	return server, err
}

func (s *Servers) UriForServer(serverAddr string) string {
	uri := &url.URL{Scheme: "http", Host: "localhost:8000", Path: "/servers/" + serverAddr}
	return uri.String()
}

func (s *Servers) getServerName(name string) []byte {
	return []byte(name)
}
