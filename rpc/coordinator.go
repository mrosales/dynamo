package rpc

import (
	"bitbucket.org/mrosales/dynamo/stash"
	"errors"
	"fmt"
	"os"
	"sync"
)

type Coordinator struct {
	stash *stash.Stash
	Key   string
}

func NewCoordinator(st *stash.Stash) *Coordinator {
	return &Coordinator{stash: st}
}

func (cd *Coordinator) CoordinatePut(key []byte, value []byte) (success bool, err error) {
	srv := cd.stash.Servers
	nodes, _ := srv.PreferenceListForKey(string(key), cd.stash.NumReplicas)
	fmt.Printf("Put(%s) -> %v\n", string(key), nodes)

	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered in Put()", r)
			err = errors.New("Unhandled Panic Occurred in Put()")
			return
		}
	}()

	responses := make(chan bool, len(nodes))

	var wg sync.WaitGroup

	selfAddr := os.Getenv("DYNAMO_ADDR")

	// issue requests for all N nodes
	wg.Add(len(nodes))
	for _, n := range nodes {
		node := n
		if node == selfAddr {
			go func() {
				resp := cd.stash.Put(key, value)
				if !resp {
					fmt.Printf("Put to self -> %t\n", resp)
				}
				responses <- resp
				wg.Done()
			}()
		} else {
			go func() {
				client := NewRPCClient("http://" + node)
				resp, err := client.PutInternal(key, value)
				if err != nil {
					fmt.Printf("PutInternal[%s] Error: %v\n", node, err)
					responses <- false
				} else {
					if !resp.Success {
						fmt.Printf("Put to %s -> %t\n", node, resp.Success)
					}
					responses <- resp.Success
				}
				wg.Done()
			}()
		}
	}

	go func() {
		// use waitgroup to close responses when all requests are finished
		// closing responses before calls are finished makes things break
		wg.Wait()
		close(responses)
	}()

	responseMap := make(map[bool]int)
	responseMap[true], responseMap[false] = 0, 0

	// waits for responses to come in from all of the nodes
	for resp := range responses {
		responseMap[resp] = responseMap[resp] + 1
		if responseMap[resp] >= cd.stash.WriteParticipants {
			return resp, nil
		}
	}
	return responseMap[true] >= responseMap[false], nil
}

func (cd *Coordinator) CoordinateGet(key []byte) (response []byte, err error) {
	nodes, _ := cd.stash.Servers.PreferenceListForKey(string(key), cd.stash.NumReplicas)
	fmt.Printf("Get(%s) <- %v\n", string(key), nodes)

	responses := make(chan string, len(nodes))

	selfAddr := os.Getenv("DYNAMO_ADDR")

	var wg sync.WaitGroup
	wg.Add(len(nodes))
	// issue requests for all N nodes
	for _, n := range nodes {
		node := n
		if node == selfAddr {
			go func() {
				val, _ := cd.stash.Get(key)
				// fmt.Printf("Get from self -> %s\n", string(val))
				v := string(val)
				responses <- v
				wg.Done()
			}()
		} else {
			go func() {
				client := NewRPCClient("http://" + node)
				resp, err := client.GetInternal(key)
				if err != nil {
					fmt.Printf("GetInternal[%s] Error: %v\n", node, err)
					// responses <- nil
					wg.Done()
				} else {
					// fmt.Printf("Get from %s -> %s\n", node, string(resp.Value))
					v := string(resp.Value)
					responses <- v
					wg.Done()
				}

			}()
		}

	}

	go func() {
		// use waitgroup to close responses when all requests are finished
		// closing responses before calls are finished makes things break
		wg.Wait()
		close(responses)
	}()

	responseMap := make(map[string]int)

	// waits for responses to come in from all of the nodes
	for resp := range responses {
		if val, ok := responseMap[resp]; ok {
			responseMap[resp] = val + 1
		} else {
			responseMap[resp] = 1
		}

		if responseMap[resp] >= cd.stash.ReadParticipants {
			return []byte(resp), nil
		}
	}

	return nil, errors.New("Could not collect a majority read response")
}
