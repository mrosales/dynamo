package rpc

import (
	"bitbucket.org/mrosales/dynamo/stash"
	"log"
	"net/http"
)

type PutArgs struct {
	Key   []byte
	Value []byte
}

type PutReply struct {
	Success bool
}

type GetArgs struct {
	Key []byte
}

type GetReply struct {
	Key   []byte
	Value []byte
}

func NewStashService(stash *stash.Stash) *StashService {
	return &StashService{stash}
}

type StashService struct {
	Stash *stash.Stash
}

// Internal facing – used by coordinator nodes to respond to requests
func (s *StashService) PutInternal(r *http.Request, args *PutArgs, reply *PutReply) error {
	reply.Success = s.Stash.Put(args.Key, args.Value)
	return nil
}

func (s *StashService) GetInternal(r *http.Request, args *GetArgs, reply *GetReply) error {
	response, err := s.Stash.Get(args.Key)
	reply.Key = args.Key
	reply.Value = response
	if err != nil {
		log.Printf("error: %+v", err)
	}
	return err
}

// External facing Put/Get -> These should trigger request coordination

func (s *StashService) Put(r *http.Request, args *PutArgs, reply *PutReply) (err error) {
	coord := NewCoordinator(s.Stash)
	success, err := coord.CoordinatePut(args.Key, args.Value)

	if err != nil {
		reply.Success = false
		return err
	}

	reply.Success = success
	return nil
}

func (s *StashService) Get(r *http.Request, args *GetArgs, reply *GetReply) error {
	coord := NewCoordinator(s.Stash)
	value, err := coord.CoordinateGet(args.Key)

	reply.Key = args.Key
	if err != nil {
		reply.Value = nil
		return err
	}

	reply.Value = value
	return nil
}
