package rpc

import (
	"bitbucket.org/mrosales/dynamo/config"
	"bitbucket.org/mrosales/dynamo/servers"
	"fmt"
	"log"
	"net/http"
)

func NewServerService(servers *servers.Servers, config *config.Config) *ServerService {
	return &ServerService{servers, config}
}

type ListServersArgs struct{}

type ListServersReply struct {
	ServerList []string
	NodeConfig config.NodeConfig
}

type UpdateServerListArgs struct {
	ServerList []string
}

type ServerArgs struct {
	Addr string
}

type ServerReply struct {
	ServerList []string
}

type ServerService struct {
	Servers *servers.Servers
	Config  *config.Config
}

func (s *ServerService) Join(r *http.Request, args *ServerArgs, reply *ServerReply) error {
	s.Servers.Add([]byte(args.Addr))

	list, err := s.Servers.List()
	if err != nil {
		log.Printf("Error retreiving server list: %+v\n", err)
	}

	if list != nil {
		reply.ServerList = list

		// iterate through existing servers in server list and update their lists
		go func() {
			ownAddr := s.Config.InstanceData.LocalIP + ":" + s.Config.InstanceData.RPC_PORT
			for _, server := range list {
				if server == ownAddr {
					continue
				}
				client := NewRPCClient("http://" + server)
				// TODO Handle failures
				fmt.Println("Updating server list for: ", server)
				client.UpdateServerList(list)
			}
		}()

	} else {
		reply.ServerList = make([]string, 0)
	}

	log.Printf("Reply: \"%s\" -> %s", args.Addr, reply.ServerList)
	return nil
}

func (s *ServerService) ListServers(r *http.Request, args *ListServersArgs, reply *ListServersReply) error {
	servers, err := s.Servers.List()
	if err != nil {
		return err
	}

	reply.ServerList = servers
	reply.NodeConfig = *s.Config.NodeConfig
	return nil
}

func (s *ServerService) UpdateServerList(r *http.Request, args *UpdateServerListArgs, reply *BoolReply) error {
	for _, server := range args.ServerList {
		fmt.Printf("Adding server: %s\n", server)
		s.Servers.Add([]byte(server))
	}

	reply.Success = true
	return nil
}
