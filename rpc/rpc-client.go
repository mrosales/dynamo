package rpc

import (
	"bytes"
	"github.com/gorilla/rpc/json"
	"net/http"
)

func NewRPCClient(url string) *RPCClient {
	return &RPCClient{url + "/rpc"}
}

type RPCClient struct {
	URL string
}

func (c *RPCClient) makeRPCRequest(methodName string, args interface{}, result interface{}) error {

	url := c.URL

	message, err := json.EncodeClientRequest(methodName, args)
	if err != nil {
		return err
	}
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(message))

	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")
	client := new(http.Client)
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	err = json.DecodeClientResponse(resp.Body, result)
	if err != nil {
		return err
	}

	return nil
}

func (c *RPCClient) GetInternal(key []byte) (*GetReply, error) {
	args := &GetArgs{key}
	var result GetReply

	err := c.makeRPCRequest("StashService.GetInternal", args, &result)
	if err != nil {
		return nil, err
	} else {
		return &result, nil
	}
}

func (c *RPCClient) PutInternal(key []byte, value []byte) (*PutReply, error) {
	args := &PutArgs{key, value}
	var result PutReply

	err := c.makeRPCRequest("StashService.PutInternal", args, &result)
	if err != nil {
		return nil, err
	} else {
		return &result, nil
	}
}

func (c *RPCClient) Get(key []byte) (*GetReply, error) {
	args := &GetArgs{key}
	var result GetReply

	err := c.makeRPCRequest("StashService.Get", args, &result)
	if err != nil {
		return nil, err
	} else {
		return &result, nil
	}
}

func (c *RPCClient) Put(key []byte, value []byte) (*PutReply, error) {
	args := &PutArgs{key, value}
	var result PutReply

	err := c.makeRPCRequest("StashService.Put", args, &result)
	if err != nil {
		return nil, err
	} else {
		return &result, nil
	}
}

func (c *RPCClient) ListServers() (*ListServersReply, error) {
	var result ListServersReply
	err := c.makeRPCRequest("ServerService.ListServers", &ListServersArgs{}, &result)

	if err != nil {
		return nil, err
	}
	return &result, nil
}

func (c *RPCClient) RegisterInstanceConfig(args *RegisterInstanceArgs) (*RegisterInstanceReply, error) {
	var result RegisterInstanceReply
	err := c.makeRPCRequest("ConfigService.RegisterInstance", args, &result)

	if err != nil {
		return nil, err
	}
	return &result, nil
}

func (c *RPCClient) InitializeInstanceConfig(args *InitializeInstanceArgs) (*InitializeInstanceReply, error) {
	var result InitializeInstanceReply
	err := c.makeRPCRequest("ConfigService.InitializeInstance", args, &result)

	if err != nil {
		return nil, err
	}
	return &result, nil
}

func (c *RPCClient) UpdateServerList(serverList []string) (*BoolReply, error) {
	args := &UpdateServerListArgs{
		ServerList: serverList,
	}
	var result BoolReply
	err := c.makeRPCRequest("ServerService.UpdateServerList", args, &result)

	if err != nil {
		return nil, err
	}
	return &result, nil
}

func (c *RPCClient) Join(serverAddr string, rpcPort string) (*ServerReply, error) {
	args := &ServerArgs{
		Addr: serverAddr + ":" + rpcPort,
	}
	var result ServerReply
	err := c.makeRPCRequest("ServerService.Join", args, &result)

	if err != nil {
		return nil, err
	}
	return &result, nil
}
