package rpc

import (
	"bitbucket.org/mrosales/dynamo/config"
	"bitbucket.org/mrosales/dynamo/servers"
	"fmt"
	"net/http"
	"strconv"
)

func NewConfigService(config *config.Config, servers *servers.Servers) *ConfigService {
	return &ConfigService{config, servers}
}

type RegisterInstanceArgs config.InstanceData
type InitializeInstanceArgs config.NodeConfig

type BoolReply struct {
	Success bool
}
type RegisterInstanceReply BoolReply
type InitializeInstanceReply BoolReply

type ConfigService struct {
	Config        *config.Config
	ServerService *servers.Servers
}

func (s *ConfigService) RegisterInstance(r *http.Request, args *RegisterInstanceArgs, reply *RegisterInstanceReply) error {
	idata := config.InstanceData(*args)

	success := s.Config.RegisterInstance(&idata)

	reply.Success = success
	return nil
}

func (s *ConfigService) InitializeInstance(r *http.Request, args *InitializeInstanceArgs, reply *InitializeInstanceReply) error {
	// cast args to NodeConfig type
	cfg := config.NodeConfig(*args)

	success := s.Config.InitializeInstance(&cfg)

	if !cfg.IsRoot {
		rootAddr := cfg.RootAddr + ":" + strconv.FormatInt(cfg.RootRPCPort, 10)
		client := NewRPCClient("http://" + rootAddr)
		fmt.Printf("Attempting to join root '%s'\n", rootAddr)

		resp, err := client.Join(s.Config.InstanceData.LocalIP, s.Config.InstanceData.RPC_PORT)
		if err != nil {
			fmt.Println("error joining root: ", err)
		} else {
			fmt.Println("Root joined. Adding server list.")
			for _, serv := range resp.ServerList {
				s.ServerService.Add([]byte(serv))
			}
		}
	} else {
		port := s.Config.InstanceData.RPC_PORT
		rootAddr := s.Config.InstanceData.LocalIP + ":" + port
		s.ServerService.Add([]byte(rootAddr))
		fmt.Println("initialized as root!")
	}

	reply.Success = success
	return nil
}
