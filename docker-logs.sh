#!/bin/sh

ssh "ec2-user@$1" "docker ps -q | head -n1 | xargs docker logs -f"