#!/bin/sh

docker build -t mrosales/cloud-dynamo:latest . && \
docker push mrosales/cloud-dynamo:latest && \
ecs-cli compose -p dynamo down && \
sh docker-up.sh